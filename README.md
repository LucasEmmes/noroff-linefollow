# Linefollow bot
This robot will follow a white line on the floor.  
That is its purpose.  
It is quite depressing, we know.

## How it works
The bot uses opencv to recognize white blobs, which are then convertet into directed lines.  
Based on the rough angle of the line, the bot will adjust its motor-speeds to follow it.